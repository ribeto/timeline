//
//  TweetCell.h
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class Tweet;

@interface TweetCell : UITableViewCell

@property (strong, nonatomic) Tweet *tweet;

@end
