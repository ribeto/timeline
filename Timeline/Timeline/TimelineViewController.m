//
//  TimelineViewController.m
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "TimelineViewController.h"

// Models
#import "Tweet.h"

// Managers
#import "TwitterManager.h"

// Views
#import "TweetCell.h"

@interface TimelineViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *tweets;

@end

@implementation TimelineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view.
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(imageDownoadedHandler:)
                                               name:ImageDownloadedNotification
                                             object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
  
  UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
  spinner.hidesWhenStopped = YES;
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
  [spinner startAnimating];
  
  __weak TimelineViewController *weakSelf = self;
  [[TwitterManager sharedManager] loadTweetsForUserHandle:self.userHandle onSuccess:^(NSArray *tweets) {
    TimelineViewController *strongSelf = weakSelf;
    
    if( strongSelf ) {
      strongSelf.tweets = tweets;
      [strongSelf.tableView reloadData];
      UIBarButtonItem *rightBarButton = strongSelf.navigationItem.rightBarButtonItem;
      [(UIActivityIndicatorView *)rightBarButton.customView stopAnimating];
    }
    
  } onFailure:^(NSError *error) {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:[error localizedDescription]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    TimelineViewController *strongSelf = weakSelf;
    if( strongSelf ) {
      UIBarButtonItem *rightBarButton = strongSelf.navigationItem.rightBarButtonItem;
      [(UIActivityIndicatorView *)rightBarButton.customView stopAnimating];
    }
    
  }];
  
  self.title = [NSString stringWithFormat:@"%@'s tweets", self.userHandle];
}

- (void)viewWillDisappear:(BOOL)animated {
  [[TwitterManager sharedManager] abortLoading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource
////////////////////////////////////////////////////////////////////////////////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.tweets count];
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate
////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  static NSString *CellIdentifier = @"TweetCellIdentifier";
  
  TweetCell *cell = (TweetCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                                 forIndexPath:indexPath];
  
  Tweet *tweet = self.tweets[indexPath.row];
  UIImage *avatarImage = [[TwitterManager sharedManager] loadImageForUrl:tweet.avatarUrl];
  tweet.avatarImage = avatarImage;

  cell.tweet = tweet;
  
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat minHeight = 55.0f;
  CGFloat padding = 20.0f;
  
  Tweet *tweet = self.tweets[indexPath.row];
  CGSize textSize = [tweet.text sizeWithFont:[UIFont systemFontOfSize:17.0f]
                           constrainedToSize:CGSizeMake(248.0f, 10000.0f)
                               lineBreakMode:NSLineBreakByWordWrapping];
  CGFloat totalHeight = 34.0f + textSize.height + padding;
  CGFloat height = totalHeight < minHeight ? minHeight : totalHeight;
  
  return height;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Notification Handlers
////////////////////////////////////////////////////////////////////////////////

- (void)imageDownoadedHandler:(NSNotification *)note {
  NSNumber *urlHash = (NSNumber *)note.object;
  NSMutableArray *pathsToReload = [NSMutableArray new];
  
  for( TweetCell *cell in self.tableView.visibleCells ) {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Tweet *tweet = self.tweets[indexPath.row];
    if( [tweet.avatarUrl hash] == [urlHash integerValue] ) {
      [pathsToReload addObject:indexPath];
    }
  }
  
  [self.tableView reloadRowsAtIndexPaths:pathsToReload withRowAnimation:UITableViewRowAnimationNone];
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAlertViewDelegate
////////////////////////////////////////////////////////////////////////////////
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  // There was an error move the user back to the username handle controller
  [self.navigationController popViewControllerAnimated:YES];
}
@end
