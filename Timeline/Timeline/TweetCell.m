//
//  TweetCell.m
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "TweetCell.h"
#import "Tweet.h"

@interface TweetCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *handleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;


@end

@implementation TweetCell

  

- (void)awakeFromNib {
  CAShapeLayer *maskLayer = [CAShapeLayer layer];
  maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.avatarImage.bounds
                                         byRoundingCorners:UIRectCornerAllCorners
                                               cornerRadii:CGSizeMake(10.0f, 10.0f)].CGPath;
  self.avatarImage.layer.mask = maskLayer;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
  [super setSelected:selected animated:animated];
  
  // Configure the view for the selected state
}

- (void)refreshUI {
  self.handleLabel.text   = self.tweet.name;
  self.tweetLabel.text    = self.tweet.text;
  self.avatarImage.image  = self.tweet.avatarImage;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Setters
////////////////////////////////////////////////////////////////////////////////

- (void)setTweet:(Tweet *)tweet {
  if( _tweet != tweet ) {
    _tweet = tweet;
    [self refreshUI];
  }
}

@end
