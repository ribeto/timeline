//
//  Tweet.m
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "Tweet.h"

@implementation Tweet

- (id)initWithDictionary:(NSDictionary *)tweetDictionary {
  self = [super init];
  if( self ) {
    
    self.name = tweetDictionary[@"user"][@"name"];
    self.text = tweetDictionary[@"text"];
    self.avatarUrl = tweetDictionary[@"user"][@"profile_image_url_https"];
    
  }
  return self;
}

@end
