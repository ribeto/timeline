//
//  Tweet.h
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tweet : NSObject

@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSString  *text;
@property (strong, nonatomic) NSString  *avatarUrl;
@property (strong, nonatomic) UIImage   *avatarImage;

- (id)initWithDictionary:(NSDictionary *)tweetDictionary;

@end
