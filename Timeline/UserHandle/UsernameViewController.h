//
//  UsernameViewController.h
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsernameViewController : UIViewController <UITextFieldDelegate>

@end
