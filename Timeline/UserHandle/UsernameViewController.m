//
//  UsernameViewController.m
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "UsernameViewController.h"
#import "TimelineViewController.h"

@interface UsernameViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;


@end

@implementation UsernameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view.
  
  [self.usernameTextfield becomeFirstResponder];
  
  self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Handle" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldDelegate
////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  NSString *handle = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  
  if( handle.length > 0 ) {
    //
    // handle is not blank go off and load the timeline
    //
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    TimelineViewController *timelineController = [storyboard instantiateViewControllerWithIdentifier:@"timeline"];
    timelineController.userHandle = handle;
    [self.navigationController pushViewController:timelineController animated:YES];
    
  } else {
    //
    // Need to have a handle
    //
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Blank Handle" message:@"The Twitter handle cannot be blank" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
  }
  
  return YES;
}

@end
