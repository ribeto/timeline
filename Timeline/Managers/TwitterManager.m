//
//  TwitterManager.m
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "TwitterManager.h"
#import "Tweet.h"
#import "Reachability.h"

@interface TwitterManager ()

@property NSOperationQueue  *queue;
@property NSCache           *cache;

@end

@implementation TwitterManager

NSString * const ImageDownloadedNotification = @"ImageDownloadedNotification";

+ (TwitterManager *)sharedManager
{
  static dispatch_once_t once;
  static TwitterManager *sharedInstance;
  dispatch_once(&once, ^ {
    sharedInstance = [[TwitterManager alloc] init];
    sharedInstance.queue = [NSOperationQueue new];
    [sharedInstance.queue setMaxConcurrentOperationCount:1];
    sharedInstance.cache = [NSCache new];
  });
  return sharedInstance;
}

////////////////////////////////////////////////////////////////////////////////

- (id)parseResponseData:(NSData *)responseData andError:(NSError *)error {
  return [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
}

- (BOOL)isTwitterReachable {
  Reachability *reachability = [Reachability reachabilityWithHostname:@"twitter.com"];
  return [reachability isReachable];
}

- (void)postErrorWithText:(NSString *)errorText toFailureBlock:(void(^)(NSError *error))failureBlock {
  NSError *error = [NSError errorWithDomain:@"timeline" code:666 userInfo:@{ NSLocalizedDescriptionKey : errorText } ];
  dispatch_async(dispatch_get_main_queue(), ^{
    failureBlock(error);
  });
}

////////////////////////////////////////////////////////////////////////////////

- (void)loadTweetsForUserHandle:(NSString*)userHandle
                      onSuccess:(void(^)(NSArray *tweets))successBlock
                      onFailure:(void(^)(NSError *error))failureBlock {
  
  static NSString *timelineUrlStr = @"https://api.twitter.com/1.1/statuses/user_timeline.json";
  
  // make sure twitter is up
  if( ![self isTwitterReachable] ) {
    NSString *errorText = @"Couldn't connect to Twitter. Make sure you have a stable internet connection and try again.";
    [self postErrorWithText:errorText toFailureBlock:failureBlock];
    return;
  }
  
  ACAccountStore *store = [ACAccountStore new];
  ACAccountType *accountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
  
  //
  // need to have an account athenticated with twitter otherwise a bad authentication is returned
  // try this in the broser
  // https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=cnn
  //
  [store requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
    
    //
    // No authenticated with a twitter account
    //
    if( !granted ) {
      NSString *errorText = @"You need to provide access to your twitter account otherwise the timeline cannot be loaded";
      [self postErrorWithText:errorText toFailureBlock:failureBlock];
      return;
    }
    
    NSArray *accounts = [store accountsWithAccountType:accountType];
    NSURL *timelineUrl = [NSURL URLWithString:timelineUrlStr];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                            requestMethod:SLRequestMethodGET
                                                      URL:timelineUrl
                                               parameters:@{@"screen_name" : userHandle}];
    request.account = accounts[0];
    NSURLRequest *urlRequest = [request preparedURLRequest];
    
    //
    // Add the loading tweets operation on the our queue
    //
    [self.queue addOperationWithBlock:^{
      NSHTTPURLResponse *response = nil;
      NSError *error = nil;
      NSData *responseData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
      NSUInteger statusCode = [response statusCode];
      
      //
      // Make sure we got a valid response in the 200 to 300 range
      //
      if( !error && (statusCode >= 200 && statusCode < 300) ) {
        NSError *jsonError = nil;
        NSArray *timelineArray = [self parseResponseData:responseData andError:jsonError];
        
        NSMutableArray *tweets = nil;
        if( !jsonError ) {
          tweets = [[NSMutableArray alloc] initWithCapacity:[timelineArray count]];
          for( NSDictionary *tweetDict in timelineArray ) {
            Tweet *tweet = [[Tweet alloc] initWithDictionary:tweetDict];
            [tweets addObject:tweet];
          }
          
          if( [tweets count] == 0 ) {
            NSString *errorText = [NSString stringWithFormat:@"The account for %@ has no tweets",userHandle];
            [self postErrorWithText:errorText toFailureBlock:failureBlock];
            return;
          }
        } else {
          //
          // There was an error parsing the server data
          //
          [self postErrorWithText:@"There was an error parsing data from the sever" toFailureBlock:failureBlock];
          return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
          successBlock(tweets);
        });
      } else {
        
        if( error == nil ) {
          // errors from twitter are not really helpful I get back 'that page does not exist'
          /*NSError *jsonError = nil;
          NSDictionary *errorDict = [self parseResponseData:responseData andError:jsonError];
          
          if( [errorDict objectForKey:@"errors"] ) {
            NSArray *errors = [errorDict objectForKey:@"errors"];
            if( [errors count] > 0 && [errors[0] objectForKey:@"message"] ) {
              error = [NSError errorWithDomain:@"timeline" code:666 userInfo:@{ NSLocalizedDescriptionKey : errors[0][@"message"]} ];
            } 
          } else {*/
            
            
          NSString *genericError = [NSString stringWithFormat:@"Couldn't load the timeline for %@. Make sure the name is spelled correctly",userHandle];
          error = [NSError errorWithDomain:@"timeline" code:666 userInfo:@{ NSLocalizedDescriptionKey : genericError} ];
        
          
        }
        
        [self postErrorWithText:[error localizedDescription] toFailureBlock:failureBlock];
      }
    }];
  }];

}

////////////////////////////////////////////////////////////////////////////////

- (UIImage *)loadImageForUrl:(NSString *)imageUrl {
  NSNumber *urlHash = [NSNumber numberWithInteger:[imageUrl hash]];
  
  UIImage *avatarImage = [self.cache objectForKey:urlHash];
  
  if( !avatarImage ) {
    // set a placeholder image
    avatarImage = [UIImage imageNamed:@"avatarPlaceholder"];
    
    //
    // No image exists for this url we need to load one from the web
    //
    [self.queue addOperationWithBlock:^{
      NSURL *url = [NSURL URLWithString:imageUrl];
      NSURLRequest *request = [NSURLRequest requestWithURL:url];
      
      NSURLResponse *response = nil;
      NSError *error = nil;
      NSData *imageData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
      
      if( !error && ![self.cache objectForKey:urlHash]) {
        UIImage *image = [UIImage imageWithData:imageData];
        
        [self.cache setObject:image forKey:urlHash];
        
        dispatch_async( dispatch_get_main_queue(), ^{
          [[NSNotificationCenter defaultCenter] postNotificationName:ImageDownloadedNotification object:urlHash];
        });
        
      }
    }];
  }

  return avatarImage;
}

////////////////////////////////////////////////////////////////////////////////

- (void)abortLoading {
  [self.queue cancelAllOperations];
}

@end
