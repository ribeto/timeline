//
//  TwitterManager.h
//  Timeline
//
//  Created by Hristo Hristov on 10/25/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface TwitterManager : NSObject

extern NSString * const ImageDownloadedNotification;

+ (TwitterManager *)sharedManager;

//
// Loads the tweets for the specified user handle.  On success executes the
// successBlock of failure executes the failureBlock
//
- (void)loadTweetsForUserHandle:(NSString*)userHandle
                      onSuccess:(void(^)(NSArray *tweets))successBlock
                      onFailure:(void(^)(NSError *error))failureBlock;

//
// Loads an image at the specified url and stores it in the cache
//
- (UIImage *)loadImageForUrl:(NSString *)imageUrl;

//
// Cancel all network operations
//
- (void)abortLoading;

@end
